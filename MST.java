public class MST {
    private static final int V = 7;

    // Viết 1 hàm tìm đỉnh với giá trị thấp nhất
    // tập hợp các giá trị từ cây đồ thị
    private int minKey(int[] key, Boolean[] mstSet)
    {
        // Khởi tạo giá trị tối thiểu
        int min = Integer.MAX_VALUE, min_index = -1;

        for (int v = 0; v < V; v++) {
            if (!mstSet[v] && key[v] < min) {
                min = key[v];
                min_index = v;
            }
        }

        return min_index;
    }

    // Hàm in ra giá trị của các đỉnh
    // parent[]
    private void printMST(int[] parent, int[][] graph)
    {
        System.out.println("Edge \tWeight");
        for (int i = 1; i < V; i++)
            System.out.println(parent[i] + " - " + i + "\t" + graph[i][parent[i]]);
    }

    // Hàm xây dựng và in ra MST cho đồ thị
    // Sử dụng ma trận kề
    private void primMST(int[][] graph)
    {
        // Xây dựng mảng để lưu trữ MST
        int[] parent = new int[V];

        // Giá trị nhỏ nhất của đồ thị
        int[] key = new int[V];

        // biểu diễn tập hợp các đỉnh
        Boolean[] mstSet = new Boolean[V];

        // Khởi tạo tất cả các giá trị (value) dưới dạng infinite
        for (int i = 0; i < V; i++) {
            key[i] = Integer.MAX_VALUE;
            mstSet[i] = false;
        }

        //
        key[0] = 0; // Tạo khóa bằng 0 để lưu trữ đỉnh đầu tiên
        parent[0] = -1; // Nút đầu tiên luôn là root của MST

        // MST sẽ có V đỉnh
        for (int count = 0; count < V - 1; count++) {
            // Chọn đỉnh từ khóa tối tiểu

            int u = minKey(key, mstSet);
            System.out.println(u);

            // Thêm đỉnh đã chọn vào
            mstSet[u] = true;

            //Cập nhật giá trị khóa và chỉ số cha của các đỉnh liền kề của đỉnh đã chọn.
            //Chỉ xem xét các đỉnh chưa được bao gồm trong MST
            for (int v = 0; v < V; v++) {

                // graph[u][v]
                // mstSet[v]
                // 	Chỉ cập nhật khóa nếu graph[u][v] nhỏ hơn khóa [v]
                if (graph[u][v] != 0 && !mstSet[v] && graph[u][v] < key[v]) {
                    parent[v] = u;
                    key[v] = graph[u][v];
                }
            }
        }

        // In cây MST
        printMST(parent, graph);
    }

    public static void main(String[] args)
    {
        /* Tạo cây đồ thị (theo mẫu dưới)
        
              1           3
        (0)--------(1)--------(2)
         |          |          |
         |          |          |
         | 4        | 5        | 8
         |          |          |
         |    6     |     7    |
        (4)--------(3)--------(6)
          \         |         /
            \       |       /
              \ 7   | 2   / 9
                \   |   /
                  \ | /
                   (5)
        
        */
        MST t = new MST();
        int[][] graph = new int[][] { 
            { 0, 1, 0, 0, 4, 0, 0 },
            { 1, 0, 3, 5, 0, 0, 0 },
            { 0, 3, 0, 0, 0, 0, 8 },
            { 0, 5, 0, 0, 6, 2, 7 },
            { 4, 0, 0, 6, 0, 7, 0 },
            { 0, 0, 0, 2, 7, 0, 9 },
            { 0, 0, 8, 7, 0, 9, 0 }
        };

        // In ra kết quả
        t.primMST(graph);
    }
}